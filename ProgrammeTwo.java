//VigenereCipher

import java.util.Scanner;

class ProgrammeTwo {

    // This function generates the key in
    // a cyclic manner until it's length isi'nt
    // equal to the length of original text
    static String encrypt(String plaintext, String key) {
        StringBuilder ciphertext = new StringBuilder();
        int keyLength = key.length();

        for (int i = 0; i < plaintext.length(); i++) {
            char plainChar = plaintext.charAt(i);
            char keyChar = key.charAt(i % keyLength);

            if (Character.isLetter(plainChar)) {
                char base = Character.isLowerCase(plainChar) ? 'a' : 'A';
                int shift = keyChar - base;
                char encryptedChar = (char) (((plainChar - base + shift) % 26) + base);
                ciphertext.append(encryptedChar);
            } else {
                ciphertext.append(plainChar); // Keep non-letter characters as they are
            }
        }

        return ciphertext.toString();
    }

    static String decrypt(String ciphertext, String key) {
        StringBuilder plaintext = new StringBuilder();
        int keyLength = key.length();

        for (int i = 0; i < ciphertext.length(); i++) {
            char cipherChar = ciphertext.charAt(i);
            char keyChar = key.charAt(i % keyLength);

            if (Character.isLetter(cipherChar)) {
                char base = Character.isLowerCase(cipherChar) ? 'a' : 'A';
                int shift = keyChar - base;
                char decryptedChar = (char) (((cipherChar - base - shift + 26) % 26) + base);
                plaintext.append(decryptedChar);
            } else {
                plaintext.append(cipherChar); // Keep non-letter characters as they are
            }
        }

        return plaintext.toString();
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter a Keyword: ");
        String key = sc.nextLine();

        System.out.print("Enter a Text for Encryption: ");
        String plaintext = sc.nextLine();

        String encryptedText = encrypt(plaintext, key);
        System.out.println("Cipher text : " + encryptedText);

        System.out.println("----------------------------------------------------------------");

        System.out.print("Enter a Text for Decryption: ");
        String text = sc.nextLine();

        String decryptedText = decrypt(text, key);
        System.out.println("Original/Decrypted Text : " + decryptedText);

        sc.close();
    }
}
