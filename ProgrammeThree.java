// Co prime

class ProgrammeThree {
    static int calculateGCD(int a, int b) {
        if (b == 0) {
            return a;
        } else {
            return calculateGCD(b, a % b);
        }
    }

    static boolean areCoprime(int a, int b) {
        int gcd = calculateGCD(a, b);
        return gcd == 1;
    }

    public static void main(String[] args) {
        int num1 = 6;
        int num2 = 12;

        boolean coprime = areCoprime(num1, num2);

        if (coprime) {
            System.out.println(num1 + " and " + num2 + " are coprime.");
        } else {
            System.out.println(num1 + " and " + num2 + " are not coprime.");
        }
    }
}
